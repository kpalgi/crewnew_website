app.factory('NodesService', function($q, $http) {
    var globalNodes;

    function getNodes() {
        var defer = $q.defer();
        if (!globalNodes)
            $http.get('/api/nodes').then(function (response) {
                globalNodes = response.data;
                defer.resolve(response.data);
            });
        else
            defer.resolve(globalNodes);

        return defer.promise;
    }

    return {
        getNodes: function () {
            return getNodes().then(function (response) {
                var data = [];
                var nodes = [];

                angular.forEach(response, function (node) {
                    var current = {
                        pagetitle: node.skill,
                        id: node.id,
                        parent: [],
                        content: node.content,
                        longtitle: node.title,
                        menutitle: node.url,
                        jobs: node.jobs,
                        providers: node.providers,
                        menuindex: node.menuindex,
                        createdon: node.createdon,
                        alias: node.alias
                    };
                    current.parent = node.parent.split(',');
                    data.push(current);
                });
                angular.forEach(data, function (node, i) {
                    if (node && node.parent == 0) {
                        nodes.push(node);
                        delete data[i];
                    }
                });

                angular.forEach(nodes, function (node, i) {
                    nodes[i].children = getChildren(node.id);
                });

                function search(a, b) {
                    console.log(a);
                    return a.indexOf(b.toString()) > -1;
                }

                function getChildren(parent) {
                    var nodes1 = [];

                    // angular.forEach(data, function (node, i) {
                    //     if (node && node.parent == parent) {
                    //         node.children = getChildren(node.id);
                    //         nodes1.push(node);
                    //         delete data[i];
                    //     }
                    // });
                    var node;
                    var hasParent;
                    for (var i = 0; i < data.length; i++) {
                        node = data[i];
                        if (node) {
                            hasParent = node.parent.indexOf(parent.toString());
                            if (hasParent > -1) {
                                node.children = getChildren(node.id);
                                nodes1.push(node);
                            }
                        }
                    }

                    return nodes1;
                }

                var deferred = $q.defer();

                function goDeeper(children) {
                    children.forEach(function (node) {
                        // var code = node.pagetitle.replace(/[^a-zA-Z+ ]/g, '');
                        // code = code.replace(/\s\s+/g, ' ');
                        // node.code = code.replace(/\s/g, '-');
                        // node.code = node.code.toLowerCase();
                        node.code = node.alias;
                        node.parent = null;
                        if (!node.children.length) {
                            deferred.resolve();
                            return 0;
                        }
                        goDeeper(node.children);
                    });
                }

                goDeeper(nodes);

                return nodes;
            });
        },
        getActiveByCategory: function(nodes, category) {
            var data;
            var deferred = $q.defer();

            function goDeeper(children, category) {
                children.forEach(function(node) {
                    if (node.code == category[0] && category[1] == undefined) {
                        data = node;
                        deferred.resolve();
                        return 0;
                    }
                    if (node.code == category[0]) {
                        category.splice(0, 1);
                        goDeeper(node.children, category);
                    }
                })
            }

            goDeeper(nodes, category);

            return data;
        }
    };
});