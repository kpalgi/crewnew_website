app.factory('QuotesService', function() {
    var quotes = [{
       title: 'RIGHT TASKS TO THE RIGHT PEOPLE',
       content: "All skills of all professionals are rated in 10 points where 10 = top expert; 8 = senior expert with years of experience; 6 = very good knowledge; 4 = can do simple tasks; 2 = can understand the basics enough to do project management..."
   },
{title: 'EMPLOYEES COME FIRST',
       content: "Clients do not come first. If you take care of your employees, they will take care of the clients. Quote by Richard Branson"},
	   {title: 'WE\'RE GOOGLE OF HR',
       content: "We develop scraper that already has scraped the LinkedIN and other biggest employment related social netowrks, freelancer sites and other platforms where professionals register. Soon we have \'em all in our database, then filtered the best talent out of the huge data, contact them and hand pick the best for you!"}];

    return {
        getQuote: function () {
            return quotes[Math.floor((Math.random() * quotes.length))];
        }
    };
});