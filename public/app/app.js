var app = angular.module('Crewnew', ['ngRoute', 'ngSanitize', 'ngAnimate', 'oitozero.ngSweetAlert']);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/partials/views/index',
            controller: 'MainCtrl'
        })
	.when('/skills', {
		redirectTo: '/c/skills'
	})
	.when('/agency-of-freelancers', {
		redirectTo: '/c/agency-of-freelancers'
	})
        .when('/c/:category*?', {
            templateUrl: '/partials/views/skills',
            controller: 'SkillsCtrl',
            noReload: true
        })
        .when('/contact', {
            templateUrl: '/partials/views/contact',
            controller: 'ContactCtrl'
        })
        .when('/martin', {
            title: 'MARTIN the cameraman',
            templateUrl: '/partials/views/martin',
            controller: 'ProfileCtrl'
        })
        .when('/martin/portrait', {
            title: 'PORTRAIT by Martin',
            templateUrl: '/partials/views/martinPortrait',
            controller: 'ProfileCtrl'
        })
        .when('/martin/architecture', {
            title: 'ARCHITECTURE by Martin',
            templateUrl: '/partials/views/martinArchitecture',
            controller: 'ProfileCtrl'
        })
        .when('/martin/pianos', {
            title: 'PIANOS by Martin',
            templateUrl: '/partials/views/martinPianos',
            controller: 'ProfileCtrl'
        })
        .when('/martin/nude', {
            title: 'NUDE by Martin',
            templateUrl: '/partials/views/martinNude',
            controller: 'ProfileCtrl'
        })
        .when('/martin/interior', {
            title: 'INTERIOR PHOTOGRAPHY by Martin',
            templateUrl: '/partials/views/martinInterior',
            controller: 'ProfileCtrl'
        })
        .when('/martin/event', {
            title: 'EVENT PHOTOGRAPHY by Martin',
            templateUrl: '/partials/views/martinEvent',
            controller: 'ProfileCtrl'
        })
        .when('/martin/business', {
            title: 'BUSINESS by Martin',
            templateUrl: '/partials/views/martinBusiness',
            controller: 'ProfileCtrl'
        })
        .when('/kaspar', {
            title: 'KASPAR project manager & PHP programmer',
            templateUrl: '/partials/views/kaspar',
            controller: 'ProfileCtrl'
        })
        .when('/marja', {
            title: 'MARJA liguistics & texts',
            templateUrl: '/partials/views/marja',
            controller: 'ProfileCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });

    $locationProvider.html5Mode(true);
});

app.run(function ($rootScope, $route, $routeParams, $window, $location) {
    $rootScope.$on('$routeChangeSuccess', function (event, current) {
        var title = current.$$route.title ? current.$$route.title : 'The New Way to Hire';
        $rootScope.title = title + ' | CrewNew.com';
    });

    $rootScope.$on('$routeChangeStart', function (event, nextRoute, lastRoute) {
        if (lastRoute && nextRoute.noReload
            && lastRoute.controller === nextRoute.controller) {
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                un();
                if (!angular.equals($route.current.params, lastRoute.params)) {
                    lastRoute.params = nextRoute.params;
                    angular.copy(lastRoute.params, $routeParams);
                    $rootScope.$broadcast('$routeUpdate', lastRoute);
                }
                $route.current = lastRoute;
            });
        }

        if (!$location.hash().length)
            $window.scrollTo(0, 0);
    });
});
