app.controller('ContactCtrl', function ($scope, $http, $location, SweetAlert) {
    window.map = {
        mapType: 'ROADMAP',
        mapZoom: 9,
        mapStyle: 'AVOIR-default',
        mapScroll: 'on',
        lat: '51.5073509',
        lng: '0.148271'
    };

    $scope.map = function (position) {
        $('.google-map').each(function () {
            var mapDiv = $(this);
            var mapData = window[mapDiv.attr('id')];

            createMap(position);

            function createMap(position) {
                var map;

                var style = [{
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{"color": "#e9e9e9"}, {"lightness": 17}]
                }, {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [{"color": "#f5f5f5"}, {"lightness": 20}]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [{"color": "#ffffff"}, {"lightness": 17}]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [{"color": "#ffffff"}, {"lightness": 29}, {"weight": 0.2}]
                }, {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [{"color": "#ffffff"}, {"lightness": 18}]
                }, {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [{"color": "#ffffff"}, {"lightness": 16}]
                }, {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{"color": "#f5f5f5"}, {"lightness": 21}]
                }, {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [{"color": "#dedede"}, {"lightness": 21}]
                }, {
                    "elementType": "labels.text.stroke",
                    "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"lightness": 16}]
                }, {
                    "elementType": "labels.text.fill",
                    "stylers": [{"saturation": 36}, {"color": "#333333"}, {"lightness": 40}]
                }, {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [{"color": "#f2f2f2"}, {"lightness": 19}]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [{"color": "#fefefe"}, {"lightness": 20}]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [{"color": "#fefefe"}, {"lightness": 17}, {"weight": 1.2}]
                }];
                var options = {
                    zoom: parseInt(mapData.mapZoom, 13),
                    center: position,
                    scrollwheel: false,
                    draggable: mapData.mapScroll === 'on',
                    mapTypeId: google.maps.MapTypeId[mapData.mapType]
                };

                map = new google.maps.Map(mapDiv[0], options);

                if (mapData.mapStyle === 'AVOIR-default') {
                    map.setOptions({
                        styles: style
                    });
                }
            }
        });
    };

    $scope.contactInfo = {};
    $scope.email = {
        loading: false
    };

    if ($location.search().email)
        $scope.contactInfo.email = $location.search().email;

    if ($location.search().martin) {
        $http({
            method: 'GET',
            url: '/api/profile/87'
        }).then(function (response) {
            var profile = response.data[0];
            $scope.profile = profile;
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': $scope.profile.town}, function(results, status) {
                if(status === google.maps.GeocoderStatus.OK) {
                    $scope.map( results[0].geometry.location );
                }
                else {
                    $scope.map(new google.maps.LatLng(window.map.lat, window.map.lng));
                }
            });
            $scope.custom = {
                intro: 'You have requested to contact ' + profile.name +'. Please, fill in bit more information about you and your needs so we can help you better. We can also contact you via phone/Skype/Slack if you prefer or meet locally near ' + profile.town +'. Are you looking for someone not around ' + profile.country +'? No problem - still fill in the form and let us know where are you based. We have great crew all around Europe & World!'
            };
        });
    } else {
        $scope.map(new google.maps.LatLng(window.map.lat, window.map.lng));
    }

    $scope.send = function (form) {
        if (form.$valid) {
            $scope.email.loading = true;
            var data = {
                email: $scope.contactInfo
            };
            data = $location.search().martin ? {
                    email: $scope.contactInfo,
                    profile: '87'
                } : data;
            console.log(data);
            $http({
                method : 'POST',
                url : '/api/email',
                data: data
            }).then(function (response) {
                SweetAlert.swal(response.data.message.header, response.data.message.body, 'success');
                $scope.email.loading = false;
                $scope.contactInfo = {};
                $scope.contactForm.$setUntouched();
                $scope.contactForm.$setPristine();
                $('html, body').animate({
                    scrollTop: $('.section-36').offset().top
                }, 'slow');
            }, function (response) {
                SweetAlert.swal(response.data.message.header, response.data.message.body, 'error');
                $scope.email.loading = false;
            });
        }
    };

    $scope.openTalk = function () {
        Tawk_API.maximize();
    };
});