app.controller('ProfileCtrl', function ($scope) {
	$('ul.masonry-wrap li .masonry-item , ul.grid-wrap li .grid-item , ul.fullscreen-wrap li .grid-item , .parallax-item').hover(
		function() {
			TweenMax.to($(this).find('img'), .01, {opacity: '.01', ease: Quart.easeOut});
			TweenMax.to($(this).find('h2'), .2, {opacity: '1', y: '-50', ease: Quart.easeOut});
			TweenMax.to($(this).find('p'), .2, {opacity: '1', y: '-30', delay: .25, ease: Quart.easeOut});
	},
		function() {
			TweenMax.to($(this).find('img'), .1, {opacity: '1', delay: .10, ease: Quart.easeOut});
			TweenMax.to($(this).find('h2'), .2, {opacity: '0', y: '0', delay: .10, ease: Quart.easeOut});
			TweenMax.to($(this).find('p'), .2, {opacity: '0', y: '0', ease: Quart.easeOut});
	});
			
	$("#myGallery").justifiedGallery({					
		rowHeight : 320,					
		fixedHeight : false, 					
		captions : false, 					
		margins : 5				
	});				
								
	$('#myGallery').photoswipe({					
		bgOpacity: 0.95,					
		loop: true				
	});

	$scope.more = function () {
		$('html,body').animate({
			scrollTop: $(".section").offset().top
		}, 'slow');
    };
});