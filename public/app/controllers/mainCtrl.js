app.controller('MainCtrl', function ($scope, $location, SweetAlert) {
    $scope.date = new Date();

    $scope.redirectToContact = function (email) {
        if (!email)
            SweetAlert.swal('Please enter your email', '', 'error');
        else {
            var profile = $location.path().split('/');
            profile =  '&' + profile[1];
            var url = '/contact?email=' + email;
            url = profile ? url + profile : url;
            $location.url(url);
        }
    };
});