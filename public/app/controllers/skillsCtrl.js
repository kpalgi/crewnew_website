app.controller('SkillsCtrl', function ($scope, $q, $location, $routeParams, $timeout, $sce, NodesService, QuotesService) {
    var activeNode = {};

    $scope.$on("$routeUpdate", function () {
        var content = $('#content');
        var downContent = $('#downContent');
        content.hide();
        downContent.html(content.html());
        getDescription();
        content.show('slide', {direction: 'right'}, 150, function () {
            downContent.html('');
        });
        var category = $routeParams.category;
        category = category.split('/');
        if (category != undefined) {
            $scope.category = [1].concat(category);
        }
    });

    $scope.scroll = function (element) {
        var e = element.substr(1);
        $location.hash(e);
        $('html, body').animate({
            scrollTop: $(element).offset().top
        }, 'slow');
    };

    function getDescription (callback) {
        var category = $routeParams.category;
        category = category ? category.split('/') : [];
        if (category[0] != "" && category.length) {
            var data = NodesService.getActiveByCategory($scope.nodes, category);
            $scope.activeNode = data;
            if (typeof $scope.activeNode.content === 'string')
                $scope.activeNode.content = $sce.trustAsHtml($scope.activeNode.content);
            activeNode.active = false;
            function setActive (nodes) {
                nodes.forEach(function (node) {
                    if (node.code == data.code) {
                        node.active = true;
                        activeNode = node;
                        return 0;
                    }
                    setActive(node.children);
                });
            }

            if (data) {
                $scope.error = null;
                setActive($scope.nodes);
                $scope.quote = QuotesService.getQuote();
            }
            else {
                $location.path('/c/');
            }
        }
    }

    function init () {
        $scope.loader = true;
        NodesService.getNodes().then(function (collection) {
            $scope.nodes = collection;
            getDescription();
            var category = $routeParams.category;
            category = category ? category.split('/') : [];
            if (category != undefined) {
                $scope.category = [1].concat(category);
            }
            $('.fa-spinner').fadeOut(500, function () {
                if ($location.hash())
                    $scope.scroll('#' + $location.hash());
            });
            $timeout(function () {
                $scope.loader = false;
            }, 450);
        });
    }

    init();
});