app.controller('NavCtrl', function ($scope, $location) {
    var sliderViews = ['/'];
    var loadedScripts = [];
    $scope.newMenuStyle = true;

    $scope.$on('$locationChangeSuccess', function () {
        $scope.newMenuStyle = $location.path()[0] == '/' && $location.path()[1] == 'c' && $location.path()[2] == '/';

        if (sliderViews.indexOf($location.path()) > -1) {
            $scope.slider = {
                visible: true
            };
            loadScript('/assets/js/slider-initialization.min.js');
        }
        else
            $scope.slider = {
                visible: false
            };
    });

    $scope.$on('$routeChangeSuccess', function () {
        function onScrollInit( items, trigger ) {
            items.each( function() {
                var osElement = $(this),
                    osAnimationClass = osElement.attr('data-os-animation'),
                    osAnimationDelay = osElement.attr('data-os-animation-delay');

                osElement.css({
                    '-webkit-animation-delay':  osAnimationDelay,
                    '-moz-animation-delay':     osAnimationDelay,
                    'animation-delay':          osAnimationDelay
                });

                var osTrigger = ( trigger ) ? trigger : osElement;

                osTrigger.waypoint(function() {
                    osElement.addClass('animated').addClass(osAnimationClass);
                },{
                    triggerOnce: true,
                    offset: '90%'
                });
            });
        }

        setTimeout(function() {
            onScrollInit( $('.os-animation') );
            onScrollInit( $('.staggered-animation'), $('.staggered-animation-container') );
        }, 600);
    });

    function loadScript(src, force) {
        var body = document.getElementsByTagName('body')[0];
        var current = false;
        var el = null;
        var scripts = document.getElementsByTagName('script');
        for (var i = 0; i < scripts.length; ++i) {
            if(scripts[i].getAttribute('src') === src) {
                el = scripts[i];
                current = true;
            }
        }

        if (force) {
            var attr = el.getAttribute('src');
            if (loadedScripts.indexOf(attr) == -1) {
                loadedScripts.push(attr);
                body.removeChild(el);
                current = false;
            }
        }

        if (!current) {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = src;
            body.appendChild(script);
        }
    }
});