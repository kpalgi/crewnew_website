app.directive('tree', function ($location, $routeParams, $rootScope) {
    return {
        scope: {
            nodes: '=',
            category: '=?',
            activeNode: '=?'
        },
        transclude: true,
        restrict: 'E',
        template: '<ul><li ng-repeat="node in nodes | orderBy: \'menuindex\'"><a ng-click="collapseNode(node)" ng-if="node.children.length"><i class="tree-indicator fa" ng-class="node.collapse ? \'fa-minus\' : \'fa-plus\'"></i></a><a href="/c/{{getParentUrl(node) + node.code}}" ng-click="showNodeDescription(node)" ng-class="{\'active\' : node.active}">{{node.pagetitle}}</a><tree ng-if="node.children.length" ng-show="node.collapse" nodes="node.children" category="category"></tree></li></ul>',
        link: function (scope, element) {
            var currentNode = {};
            scope.activeNode = scope.activeNode || {};

            scope.$watch('category', function () {
                scope.nodes.forEach(function (node) {
                    if (scope.category[scope.category[0]] != undefined && scope.category[scope.category[0]] == node.code) {
                        var n = node;
                        n.collapse = false;
                        scope.collapseNode(n);
                        scope.category[0]++;
                    }
                });
            });

            scope.collapseNode = function (node, force) {
                node.collapse = !node.collapse;
                if (force)
                    node.collapse = true;
                node.children.forEach(function (n) {
                    var parent = node.parent || node.code;
                    if (node.parent == parent)
                        parent += '/' + node.code;
                    if (n.parent)
                        n += '/' + parent;
                    else
                        n.parent = parent;
                });
                if (currentNode != node) {
                    currentNode.collapse = false;
                    currentNode = node;
                }
            };

            scope.showNodeDescription = function (node) {
                scope.collapseNode(node, 1);
            };

            scope.getParentUrl = function (node) {
                if (!node.parent)
                    return '';
                return node.parent  + '/';
            };

            if (scope.category)
                scope.nodes.forEach(function (node) {
                    if (scope.category[scope.category[0]] != undefined && scope.category[scope.category[0]] == node.code) {
                        scope.collapseNode(node);
                        scope.category[0]++;
                    }
                });
        }
    };
});