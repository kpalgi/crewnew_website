var express = require('express');
var router = express.Router();
var treeController = require('../controllers/treeController');
var emailController = require('../controllers/emailController');
var profileController = require('../controllers/profileController');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api/nodes', treeController.getNodes);

router.post('/api/email', emailController.send);

router.get('/api/profile/:id', profileController.getProfile);

router.get('/partials/:partialArea/:partialName', function(req, res) {
    res.render('../public/app/' + req.params.partialArea + '/' + req.params.partialName);
});

router.all('/*', function (req, res) {
    res.render('index');
});


module.exports = router;
