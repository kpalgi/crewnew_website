var connection = require('../config/mysql');

exports.findAllNodes = function(callback) {
    connection.query('SELECT id, skill, content, parent, url, jobs, title, providers, menuindex, alias from `cn_skills`', function (err, data) {
        console.log(data);
        if (err) callback(err);
        else callback(null, data);
    });
};