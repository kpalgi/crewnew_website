var node = require('./nodeController.js');

exports.getNodes = function (req, res) {
    node.findAllNodes(function (err, data) {
        if (err) throw err;
        return res.json(data);
    });
};