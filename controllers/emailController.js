var async = require('async');
var validator = require('validator');
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport('smtps://noreply@crewnew.com:).RLSkZ;sUJW8EuE@uk2.fcomet.com');
var emailCheck = require('email-check');
var connection = require('../config/mysql');

var successResponse = {
    message: {
        header: 'Thanks for the email!',
        body: 'We will require max. 1 business day to reply!'
    }
};
var failedResponse = {
    message: {
        header: 'Oops..',
        body: 'Something went wrong.. Please try again later!'
    }
};
var wrongEmailResponse = {
    message: {
        header: 'Oops..',
        body: 'Please enter a valid email address!'
    }
};
var contactSupportResponse = {
    message: {
        header: 'Oops..',
        body: 'We weren\'t able to establish a connection to your e-mail provider! You can contact our support team on support@crewnew.com'
    }
};

exports.send = function (req, res) {
    var email = req.body.email;
    var profile = req.body.profile;
    var userInfo = '';

    for (var field in email) {
        userInfo += '<li><b>' + field.charAt(0).toUpperCase() + field.slice(1).toLowerCase() + ': </b>' + email[field] + '</li>';
    }

    var emailContent = '<h2>New e-mail from contact form (<a href="https://crewnew.com" target="_blank">crewnew.com</a>)</h2><br/><ul style="list-style-type: none">' + userInfo +'</ul>';

    var mailOptions = {
        from: '"Contact Form 👥" <noreply@crewnew.com>',
        to: 'info@crewnew.com',
        subject: 'New e-mail from contact form',
        html: emailContent
    };

    async.series([
        function (done) {
            if (!validator.isEmail(email.email)) {
                return res.status(422).send(wrongEmailResponse);
            }
            done();
        }, function (done) {
            if (process.env.NODE_ENV)
                emailCheck(email.email)
                    .then(function (result) {
                        if (result === false)
                            return res.status(422).json(wrongEmailResponse);
                        return done();
                    })
                    .catch(function (err) {
                        if (err.message === 'refuse')
                            return res.status(506).json(contactSupportResponse);
                        else
                            return res.status(503).json(failedResponse);
                    });
            else
                return done();
        }, function (done) {
            if (profile)
                connection.query('SELECT email from `cn_main` WHERE crewId=' + profile, function (err, data) {
                    if (err) throw err;
                    profile = data[0].email;
                    mailOptions.to += ', ' + profile;
                    return done();
                });
            else
                return done();
        }, function (done) {
            transporter.sendMail(mailOptions, function(err){
                if (err){
                    return res.status(503).send(failedResponse);
                }
                return done();
            });
        }
    ], function (done) {
        return res.json(successResponse);
    });
};