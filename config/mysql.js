var mysql = require('mysql');
var env = process.env.NODE_ENV || 'development';
var config = require('../config/config.js')[env];
var connection = mysql.createConnection(config);

connection.connect(function (err) {
    if (err)
        throw err;
});

module.exports = connection;